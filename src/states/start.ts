import * as Phaser from 'phaser';

export default class StartState extends Phaser.Game {

    preload() {
        console.log("Hello this is startState");
        var style = { font: "65px Arial", fill: "#7c7c7c", align: "center" };
        let loadText = this.add.text(this.world.centerX, this.world.centerY, "Loading...", style);
        loadText.anchor.setTo(0.5, 0.5);
    }

//constructor() {

 //   let text = this.add.text(this.world.centerX, this.world.centerY, 'loading', { font: '16px Arial', fill: '#dddddd', align: 'center' });
 //   text.anchor.setTo(0.5, 0.5);
}