import * as Phaser from 'phaser';
import StartState from './states/start';
//import InGameSate from './states/inGame';
//import EndState from './states/end';

export class Breakout extends Phaser.Game {

    constructor() {

        super();

		console.log("Hello, World! This is BREAKOUT");
		this.state.add('Start', StartState, false);
		//this.state.add('InGame', InGameState, false);
		//this.state.add('End', EndState, false);

		this.state.start('Start');
		console.log("Entered Start State");

	}
}